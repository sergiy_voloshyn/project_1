package net7.xyz.photosearch;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.camera2.CameraManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.provider.MediaStore.AUTHORITY;

public class MainActivity extends AppCompatActivity {

    String mCurrentPhotoPath;

    Uri photoURIGlobal = null;

    public static final int REQUEST_IMAGE_CAPTURE = 5;
    @BindView(R.id.imageView)
    ImageView imageView;

    @BindView(R.id.fab)
    FloatingActionButton floatingActionButton;


    @OnClick(R.id.fab)
    public void OnClickFAB() {

        startPhotoActivity();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            // Show the thumbnail on ImageView
            //Uri imageUri = Uri.parse(mCurrentPhotoPath);
            //File file = new File(imageUri.getPath());
            try {
                //InputStream ims = new FileInputStream(file);
                //ivPreview.setImageBitmap(BitmapFactory.decodeStream(ims));

                //imageView.setImageURI(photoURIGlobal);
                setPic();
            } catch (Exception e) {
                Log.d("image", "onActivityResult: " + e.getMessage());
                return;
            }

        }
    }


    private void setPic() {
        // Get the dimensions of the View
        int targetW = imageView.getWidth();
        int targetH = imageView.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        //bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        imageView.setImageBitmap(bitmap);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

            //          Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            //if permission already granted or api < 23 (without runtime permission)


            //galleryAddPic();


        } else {
            //if permission not granted
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, REQUEST_IMAGE_CAPTURE);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //if we get permission
        if (REQUEST_IMAGE_CAPTURE == requestCode && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v("log", "Permission: " + permissions[0] + "was " + grantResults[0]);
            startPhotoActivity();
        }
    }


    public void startPhotoActivity() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent


        String photoFileName = "temp.jpg";
        File photoFile = null;

        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {


            try {
                //  photoFile = createImageFile();
                createImageFile();
                photoFile = new File(mCurrentPhotoPath);

            } catch (IOException ex) {
                // Error occurred while creating the File

                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Alert")
                        .setMessage(ex.toString())
                        .setCancelable(true)
                        .show();

            }
            // Create the File where the photo should go

            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "net7.xyz.photosearch.fileprovider", photoFile);
                photoURIGlobal = photoURI;
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);


                //   photoFile = new File(mCurrentPhotoPath);

            }
        }


    }

    private void createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("MMdd_HHss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        //  return image;
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }


        /*Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
        */


      /*  File fis = null;
        FileOutputStream fos = null;

        try {
            File path = new File (Environment.getExternalStorageDirectory(), "CameraTest");
            if (! path.exists()){
                path.mkdirs();{

                }
            }

            fis = new File(path+"/temp.jpg");

            //fos = openFileOutput("temp.jpg", Context.MODE_PRIVATE);
            //fos.close();

            Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            Uri outputUri=Uri.fromFile(fis);
            intent.putExtra(MediaStore.EXTRA_OUTPUT,outputUri);

          //  Intent i=new Intent(Intent.ACTION_VIEW, FileProvider.getUriForFile(this, AUTHORITY, fis));

           startActivityForResult(intent, PHOTO_INTENT_REQUEST_CODE);

            //i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            //startActivity(i);




        } catch (Exception ex) {
            ex.printStackTrace();
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Alert")
                    .setMessage(ex.toString())
                    .setCancelable(true)
                    .show();
        } finally {
            try {
                if (fis != null) {

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

     //   File file=new File(Environment.getExternalStorageDirectory(),"temp.jpg");



*/


}
